#import "@preview/cetz:0.0.1"

// Global settings
#let resume_header_height = 2cm
#let resume_side_bar_width = 8cm
#let resume_footer_height = 3.5cm

#let main_font_size = 9pt
#let h1_font_size = 15pt
#let h2_font_size = 11pt

#let verso_left_column_height = 17cm

#let primary_color = rgb("#8ec084")
#let secondary_color = rgb("#d46d03")

// Styling helper functions
#let highlight_primary_color(contents) = {
  set text(primary_color)
  contents
}

#let highlight_secondary_color(contents) = {
  set text(secondary_color)
  contents
}

#let badge(contents, color) = {
  set text(white, weight: "bold")
  box(fill:color, radius: 5pt, inset: 1mm, baseline: 1mm, contents)
}
#let stroked_badge(contents, color) = {
  set text(weight: "bold", size: 0.8em, style: "italic")
  box(stroke:0.5pt + color, radius: 5pt, inset: 1mm, baseline: 1mm, contents)
}
#let primary_badge(contents) = {
  stroked_badge(contents, primary_color)
}
#let secondary_badge(contents) = {
  stroked_badge(contents, secondary_color)
}

#let inlined_list(
  color: primary_color,
  ..items
  ) = {
  strong([
    #set text(color)
    *|->*
  ])
  items.pos()
  .join([
    #set text(color)
    *|* 
  ])
  strong([
    #set text(color)
    *---*
  ])
}

// Drawing a progress bar with different colors based on a ratio.
#let progressbar(ratio, width: 100%, height: 0.4em, fg_color: black, bg_color: white) = {
    let fg = width * ratio;
    let bg = width - fg;

    // the leading arc
    let arc1 = cetz.canvas({
        import cetz.draw: *
        arc(
            (0,0), start: 90deg, delta: 180deg, radius: height / 2, fill: if ratio == 0 { bg_color } else { fg_color }
        )
    }) 
    // the ending arc
    let arc2 = cetz.canvas({
        import cetz.draw: *
        arc(
            (0,0), start: 270deg, delta: 180deg, radius: height / 2, fill: if ratio == 1 { fg_color } else { bg_color }
        )
    })
    // frontground rectangle
    let fg = rect(
        width: fg, height: height, fill: fg_color, stroke: (left: none, right: none, rest: 1pt)
    )
    // background rectangle
    let bg = rect(
        width: bg, height: height, fill: bg_color, stroke: (left: none, right: none, rest: 1pt)
    )
    
    stack(dir: ltr, arc1, fg, bg, arc2)

}

// Mid-level styling helper functions
#let make_contact_details(details) = [
  #let table_args = ()
  #for (key, value) in details {
    table_args.push([*#upper[#{key}]*])
    table_args.push([_#{value}_])
  }
  #v(-5pt)
  #table(
    inset: 5pt,
    row-gutter: -0.25em,
    columns: (2fr, 3fr),
    align: left + horizon,
    stroke: none,
    ..table_args,
  )
]

#let job_experience(
  job_title: [],
  job_company: [],
  job_location: [],
  job_date: [],
  job_description: []
) = {
  heading(level: 2, job_title)
  par[#highlight_secondary_color([_#{job_company}_, _#{job_location}_]) #h(1fr) | #highlight_primary_color([_#{job_date}_])]
  job_description
}

#let diploma(
  title: [],
  mention: none,
  date: [],
  topic: [],
  school: []
) = {
  par[
      #text(title, weight: "bold", size: 1.2em)
      #h(1fr)
      #if not(mention == none) {
        secondary_badge[#mention]
      }
      | #text(white, date)
      #linebreak()
      *#topic*
      #linebreak()
      #set text(size: 0.85em, style: "italic")
      #school 
  ]
}

#let format_language(
    name: [],
    details: none,
    progress: 0.0,
) = {
  [== #name #h(1fr) #box(baseline: -3pt)[#progressbar(progress, height: 0.3em, width: 50%)]]
  [_#{details}_]
}

// Resume main block
#let make_header(contents) = {
  place(top + right,
    block(
      width: 100%,
      height: resume_header_height,
      fill: black,
      {
        set text(white, size: 0.8cm, )
        set align(center + horizon)
        upper(contents)
      }
    )
  )
}

#let section(title: [], contents: []) = {
  set align(center)
  heading(level: 1, upper(title))
  line(length: 1cm, stroke: 0.5pt,)
  set align(left)
  v(-1em)
  contents
}

#let make_recto_main_column(contents) = {
  place(top + right,
    dy: resume_header_height,
    block(
      width: 100%-resume_side_bar_width,
      height: 100%-resume_header_height,
      inset: 1cm, 
      fill: white,
      {
        contents
      }
    )
  )
}

#let make_recto_side_bar(avatar, summary, extra_contents) = {
  place(top + left,
    dy: resume_header_height,
    block(
      width: resume_side_bar_width,
      height: 100%-resume_header_height,
      inset: 1cm, 
      fill: primary_color,
      {
        set align(center)

        // Avatar / photo
        if not(avatar == none) {
          box(
            width: 4.5cm,
            height: 4.5cm,
            radius: 50%,
            clip: true,
            stroke: 1pt + white,
          )[
            #align(center)[#image(avatar, height: 100%)]
          ]
          linebreak()
        }

        // Summary
        par[
          #set text(white)
          </
          #set text(style: "italic")
          #set text(black)
          #summary
          #set text(white)
          />
        ]

        v(1fr)
        // Additional contents
        extra_contents
      }
    )
  )
}

#let make_footer(contents) = {
  place(bottom + right,
    dx: -resume_footer_height,
    block(
      inset: 0.8cm,
      width: 100% - resume_side_bar_width - resume_footer_height,
      height: resume_footer_height,
      fill: rgb("#dcdcdc"),
      {
        set text(8pt)
        set align(center + horizon)
        contents
      }
    )
  )
  place(bottom + right,
    rect(
      height: resume_footer_height,
      width: resume_footer_height,
      fill: secondary_color
    )
  )
}

#let make_verso_single_column(contents) = {
  place(top + left,
    dy: resume_header_height,
    block(
      width: 100%,
      height: 100% - resume_header_height - verso_left_column_height,
      inset: 1cm, 
      fill: white,
      {
        contents
      }
    )
  )
}

#let make_verso_side_column(contents) = {
  place(bottom + left,
    block(
      width: resume_side_bar_width,
      height: verso_left_column_height,
      inset: 1cm, 
      fill: primary_color,
      {
        contents
      }
    )
  )
}

#let make_verso_main_column(contents) = {
  place(bottom + right,
    dy: -resume_footer_height,
    block(
      width: 100%-resume_side_bar_width,
      height: verso_left_column_height - resume_footer_height,
      inset: 1cm, 
      fill: white,
      {
        contents
      }
    )
  )
}

#let resume(
  title: none,
  name: [],
  photo: none,
  summary: [],
  font_size: 8pt,
  h1_font_size: 15pt,
  h2_font_size: 11pt,
  recto_main_column: [],
  recto_side_column: [],
  verso_single_column: [],
  verso_side_column: [],
  verso_main_column: [],
  footer: [],
  doc,
) = {
  // Page formatting
  set page(
    paper: "a4",
    margin: (x: 0cm, y: 0cm),
  )

  show heading.where(
    level: 1
  ): it => block(width: 100%)[
    #set align(center)
    #set text(h1_font_size, weight: "semibold")
    #it.body
  ]

  show heading.where(
    level: 2
  ): it => block(width: 100%)[
    #set text(h2_font_size, weight: "bold")
    #v(1em, weak: false)
    #v(0.5em, weak: true)
    #it.body
  ]

  set par(
    justify: true,
  )

  set text(font: "Victor Mono", style: "normal", size: main_font_size)

  // Recto page
  make_header(name)
  make_recto_main_column(recto_main_column)
  make_recto_side_bar(
   photo,
   summary,
   recto_side_column,
  )
  make_footer(footer)

  // Verso page
  pagebreak()
  make_header(name)
  make_verso_single_column(verso_single_column)
  make_verso_side_column(verso_side_column)
  make_verso_main_column(verso_main_column)
  make_footer(footer)
}