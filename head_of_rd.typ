#import "template.typ": *

// Contact, summary
#let contact_details = (
  phone: [+33#h(0.3em)6#h(0.3em)47#h(0.3em)60#h(0.3em)18#h(0.3em)82],
  email: [#link("mailto:matthieu.urvoy@gmail.com")[matthieu.urvoy#h(0.3em)\@#h(0.3em)gmail.com]],
  linkedin: [#link("https://www.linkedin.com/in/murvoy/")[/in/murvoy/]],
  research: [#link("https://scholar.google.com/citations?user=eL32l0oAAAAJ")[Publications] #h(1fr)</>#h(1fr)#link("https://orcid.org/0000-0002-8445-061X")[Peer review]]
)

#let summary = [
  I'm an *engineer/researcher* with experience in *academy* and *industry*. I love teamwork, and driving the R&D process in industrial contexts. I'm specialized in *signal processing*, *computer vision*, and *artificial intelligence*. I'm also an *experienced developer* (`C++`/`Python`), from prototyping to production code.
]

// Job experience
#let job_xp_bluecime = job_experience(
  job_title: [Head of Research & Development],
  job_company: [#link("www.bluecime.com")[Bluecime]],
  job_location: [Grenoble, France],
  job_date: [2016 -- present],
  job_description: [
    #par[
      #inlined_list(
        [*Context:* Mountain-related video analysis products],
        [Prototyping & production code (`C++`) for computer vision, machine learning, deep-learning],
        [Small team of research engineers & developers],
        [Collaboration with product manager and marketing],
      )
    ]
  
    * Projects #primary_badge[MIVAO `(Fr, FUI)`] + #primary_badge[SOFTEN `(FR, i-Démo)`]:* collaboration with academic labs (GIPSA-Lab, Hubert Curien Lab, G-SCOP).
    #v(-0.5em)
    #par[
      #set text(size: 0.85em)
      #inlined_list(
        [*Side tasks*],
        [Contacts with clients and prospects to discuss new functionalities],
        [Contribution to the development of operational tools (CI/CD, product deployment, etc)],
        [Communication and graphic design (commercials)],
        [Fieldwork (work at height) to install systems in ski resorts],
      )
    ]
  ]
)

#let job_xp_chu = job_experience(
  job_title: [Research engineer],
  job_company: [#link("https://irmage.univ-grenoble-alpes.fr/")[Univ. Hospital, IRMaGe, IRM3T]],
  job_location: [Grenoble, France],
  job_date: [2014 - 2016],
  job_description: [
    *Project #primary_badge[#link("https://anr.fr/Project-ANR-12-EMMA-0056")[ERATRANIRMA `(Fr, ANR)`]]* *:* MRI medical imaging: registration of antenna signals, to optimize the reconstruction pipeline, hence increasing the image quality (e.g. robustness to patient motion).
    #v(-0.5em)
    #par[
      #set text(size: 0.85em)
      #inlined_list(
        [Preclinical environment (collaboration with physicists, neurologists, psychophysicists, radiologists, etc)],
        [Evaluation on fMRI data],
        [Experiments on phantoms and human patients],
        [Challenging software development (real-time constraints, high acquisition frequencies, mix of `C++`/`C#`/`CLI`)],
      )
    ]
  ]
)

#let job_xp_nantes = job_experience(
  job_title: [Postdoctoral researcher],
  job_company: [#link("http://www.ls2n.fr/")[Lab of digital sciences (LS2N)]],
  job_location: [Nantes, France],
  job_date: [2011 - 2014],
  job_description: [
    *Project #primary_badge[#link("https://itea4.org/media/projet-europ-en-de-recherche-jedi-m-daille-d-or-itea-2012.html")[StreamMaster, `(Fr, OSEO)`]]:* image watermarking algorithm robust to print & scan. Watermark invisbility (human visual system).
    #v(-0.5em)
    #par[
      #set text(size: 0.85em)
      #inlined_list(
        [Constrast sensitivity],
        [Fast implementation, industrial context (`C++`)]
      )
    ]

    *Project #primary_badge[#link("https://itea4.org/media/projet-europ-en-de-recherche-jedi-m-daille-d-or-itea-2012.html")[JEDI `(EU, ITEA2)`]]:* research on visual fatigue/(dis)comfort  with 3D stereoscopic displays.
    #v(-0.5em)
    #par[
      #set text(size: 0.85em)
      #inlined_list(
        [Subjective test design, setup and analysis],
        [Supervision of the making of a standardized test room],
        [Capture of 3D contents with a professional camera],
        [Electrophysiological measurements (EEG, EMG, EOG)]
      )
    ]
  ]
)

#let job_xp_phd = job_experience(
  job_title: [Doctoral researcher (industrial PhD, CIFRE)],
  job_company: [Orange (France Telecom R&D)],
  job_location: [Rennes, France],
  job_date: [2007 - 2010],
  job_description: [
    Towards a new spatiotemporal representation for video compression, called _motion tubes_.
    #v(-0.5em)
    #par[
      #set text(size: 0.85em)
      #inlined_list(
        [Semantic-friendly data structure],
        [Dedicated motion estimation and compensation solution],
        [Block-based compression with source coding],
        [Integration and evaluation within a reference `H.264` implementation (`C++`)],
      )
    ]
  ]
)

#let work_experiences = section(
  title: "R&D experience",
  contents: [
    #job_xp_bluecime
    #job_xp_chu
    #job_xp_nantes
    #job_xp_phd
  ]
)

// Languages
#let languages = section(title: "Languages", contents: [
  #format_language(
    name: "French",
    details: "Mother tongue",
    progress: 1.0,
  )
  #format_language(
    name: "English",
    details: "Fluent (2007: 12 months in Glasgow)",
    progress: 0.8,
  )
])

// Education
#let diploma_phd = diploma(
  title: [Ph.D.],
  mention: [Very honorable],
  date: [2011],
  topic: [Signal & Image processing],
  school: [Eu. Univ. of Brittany (UEB), Rennes, Fr]
)

#let diploma_meng = diploma(
  title: [M.Eng.],
  mention: [With distinction],
  date: [2007],
  topic: [Electronics and Electrical],
  school: [Univ. of Strathclyde, Glasgow, UK]
)

#let diploma_eng = diploma(
  title: [Eng. degree],
  date: [2007],
  topic: [Electronic and Industrial computing],
  school: [National Inst. of Applied Sciences (INSA), Rennes, Fr]
)

#let education = section(
  title: "Education",
  contents: [
    #v(1.5em)
    #diploma_phd
    #diploma_meng
    #diploma_eng
  ]
)

// Team work
#let team_work = section(
  title: "Team work",
  contents: [
    == Internal collaboration

    #inlined_list(
      color: white,
      [*Management*],
      [Trello],
      [Airtable]
    )

    #inlined_list(
      color: white,
      [*Software development*],
      [Collaborative tools (Git, Gitlab)],
      [Code reviews],
      [Work-in-pairs],
      [Code quality (linting, technical debt assessment)],    
    )

    == Collaborative projects

    Academic & industrial; regional, national & european.

    #set text(size: 0.85em)
    #inlined_list(
      color: white,
      [Funding applications],
      [Structuring (work packages)],
      [Consortium meetings],
    )
  ]
)

// Scientific skills
#let image_processing_skill = [
  == Image processing
  #set text(size: 0.85em)
  #inlined_list(
    [Filtering],
    [Pattern recognition],
    [Motion estimation],
    [Watermarking],
    [Depth estimation],
  )
]
#let psychophysics_skill = [
  == Psychophysics
  #set text(size: 0.85em)
  #inlined_list(
    [Human visual system],
    [Contrast sensitivity],
    [Visual fatigue and (dis)comfort],
    [Stereopsis],
  )
]
#let experimentation_skill = [
  == Experimentation
  #set text(size: 0.85em)
  #inlined_list(
    [Subjective testing],
    [Test protocols],
    [Statistical analysis],
    [Standardized room setup],
  )
]
#let medical_imaging_skill = [
  #set text(size: 0.85em)
  == Medical imaging
  #inlined_list(
    [Magnetic Resonance Imaging (MRI)],
    [Electrophysiological measurement (EEG, EOG, EMG)],
  )
]
#let telecommunication_skill = [
  == Telecommunications
  #set text(size: 0.85em)
  #inlined_list(
    [Signal processing],
    [Information theory (source and channel coding)],
    [Compression standards],
  )
]
#let maths_skill = [
  == Applied mathematics
  #set text(size: 0.85em)
  #inlined_list(
    [Adaptive filtering],
    [Detection-estimation],
    [Optimization (gradient descents)],
    [Dimensionality reduction],
  )
]
#let deep_learning_skill = [
  == Deep learning
  #set text(size: 0.85em)
  #inlined_list(
    [Convolutional Neural Networks],
    [Object detection],
    [Classification],
    [Regression],
  )
]
#let data_science_skills = [
  == Data science #h(1fr)
  #progressbar(5/8)
  #set text(size: 0.85em)
  #inlined_list(
    [`Polars`],
    [`Pandas`],
    [`R`],
  )
]

#let scientific_skills = section(
  title: "Scientific skills",
  contents: [
    #v(0.5em)
    #grid(
      gutter: 0.5cm,
      row-gutter: 0.2cm,
      columns: 4,
      image_processing_skill,
      psychophysics_skill,
      experimentation_skill,
      medical_imaging_skill,
      telecommunication_skill,
      maths_skill,
      deep_learning_skill,
    )
    
  ]
)

// Development skills
#let languages_skills = [
  == Languages
  #set text(size: 0.85em)
  #progressbar(0.9)
  #inlined_list(
    [`C++`],
    [`Python`],
    [`C#` (.Net)],
    [`Matlab`],
    [Frameworks: _OpenCV_, _OpenVINO_, _Qt_, _Intel MKL+IPP_]
  )
]
#let data_science_skills = [
  == Data science
  #set text(size: 0.85em)
  #progressbar(5/8)
  #inlined_list(
    [`Polars`],
    [`Pandas`],
    [`R`],
  )
]
#let web_skills = [
  == Web development
  #set text(size: 0.85em)
  #progressbar(4/8)
  #inlined_list(
    [`HTML`],
    [`CSS`],
    [`JS`],
    [Frameworks: _Angular_, _Flask_],
  )
]
#let devops_skills = [
  == DevOps, CI/CD
  #set text(size: 0.85em)
  #progressbar(4/8)
  #inlined_list(
    [`Gitlab`],
    [`Jenkins`],
    [`Ansible`],
    [`Docker`],
    [`Databases`],
  )
]
#let development_skills = section(
  title: "Development skills",
  contents: [
    #v(0.5em)
    #grid(
      gutter: 0.5cm,
      columns: 4,
      languages_skills,
      data_science_skills,
      web_skills,
      devops_skills
    )
    
  ]
)

// Communication
#let communication_skills = section(
  title: "Communicating research",
  contents: [
    #v(0.5em)
    #grid(
      gutter: 0.5cm,
      columns: 2,
      [
        == Dissemination
        #inlined_list(
          [(Re)distribution of software tools (academy and industry)],
          [Collaboration with research valorisation and technology transfer services (SATT)],
          [Collaboration with patent offices],
        )
      ],
      [
        == Communication
        *Written:* Journal/conference articles, project deliverables, documentation 
        #linebreak()
        *Graphical:* (Non-)commercial logos, posters & videos
        #linebreak()
        *Oral:* Presentations, workshops, conferences
      ]
    )

  ]
)

// Teaching
#let teaching = section(
  title: "Teaching & mentoring",
  contents: [
    == Teaching [_Tutorials & practical work_] #h(1fr)#text(size: main_font_size, [| #highlight_primary_color[_2007 -- 2013_]])
    
    \~ 140h, Bachelor & Masters levels, INSA Rennes & Polytech Nantes
    #set text(size: 0.85em)
    #linebreak()
    #inlined_list(
      [VHDL],
      [Signal & image processing],
      [Databases],
      [Detection, estimation],
      [Measures & experiments],
      [Acquisition & restitution interfaces]
    )

    == Theses + internships (co-)supervision

    #highlight_primary_color[*2009 -- 2013:*] programming projects (Master)
    #linebreak()
    #highlight_primary_color[*2011 -- 2013:*] research Masters
    #linebreak()
    #highlight_primary_color[*2017 -- 2020:*] PhD candidate (co-)supervision
    #linebreak()
    #highlight_primary_color[*2022 -- 2023:*] engineering Master internships

    == Volunteer teaching#h(1fr)#text(size: main_font_size, [| #highlight_primary_color[_2018 -- present_]])
    Training on snow, avalanches, cartography, orientation, human factors, etc.
  ]
)

// Hobbies and volunteering
#let hobbies = section(
  title: "Hobbies",
  contents: [
    == Sports
    #inlined_list(
      color: white,
      [Sailing],
      [Cycling],
      [Ski touring],
      [Climbing],
      [Mountaineering],
    )

    == Music
    #inlined_list(
      color: white,
      [Piano (_15+ years of classes_)],
      [Jazz, classical and electronic music enthusiast],
    )
  ]
)

#let volunteering = section(
  title: "Volunteering",
  contents: [
    == French Alpine Club (_Grenoble_)
    #inlined_list(
      color: white,
      [Ski touring guide],
      [Snow, avalanches, cartography and orientation instructor]
    )

    == Grenoble SE, sailing section
    #inlined_list(
      color: white,
      [Windsurf instructor],
      [Board-member],
    )
  ]
)

// Main document
#show: doc => resume(
  title: [Head of R&D],
  name: [Matthieu Urvoy],
  photo: "avatar.jpg",
  summary: summary,
  recto_main_column: [
    #work_experiences
  ],
  recto_side_column: [
    #education
    #team_work
  ],
  verso_side_column: [
    #languages
    #hobbies
    #volunteering
  ],
  verso_single_column:[
    #scientific_skills
    #development_skills
  ],
  verso_main_column: [
    #communication_skills
    #teaching
  ],
  footer: [
    #make_contact_details(contact_details)
  ],
  doc,
)

