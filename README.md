This is _Matthieu Urvoy_'s repository for versioning/generating his resumes. It uses [Typst](https://github.com/typst/typst).

# CI/CD

Compiling and publishing the resume is made automatic at every push.

Gitlab pages is used to host the final document:
* `head_of_rd.typ` => [https://murvoy.gitlab.io/resume/head_of_rd.pdf](https://murvoy.gitlab.io/resume/head_of_rd.pdf)

# Run locally

* Install `typst`
* Clone the repository + cd into.
* Run `typst -v compile --font-path ./fonts {file}.typ`

# My Typst template

The file `template.typ` provides the construction elements for my template.
It's not intended for generic use, with a well formatted API.
Instead, it relies on a few formatting functions to ensure consistent formatting throughout the document, while letting the user provided custom `Typst` code.


# Credits

* Thanks [TODO](TODO) for the resume template inspiration.
* Thanks [Victor Mono](https://rubjo.github.io/victor-mono/) for the excellent font.
* Thanks [123marvin123](https://hub.docker.com/r/123marvin123/typst) for the docker image.
* Thanks [Ivan Sanchez](https://gitlab.com/IvanSanchez/gitlab-ci-typst) for the gitlab integration.